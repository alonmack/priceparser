import pandas as pd

xls_goods = pd.ExcelFile("1c_ goods.xlsx")

sheet_goods = xls_goods.parse(0)
sheet_goods = sheet_goods.drop_duplicates(subset='Ключ наименование', keep='first')
sheet_goods = sheet_goods[['Ключ наименование', 'Код', 'Наименование', 'Код Валюты (0="ГРН" 1="USD" 2="EUR")']]
sheet_goods = sheet_goods[sheet_goods['Ключ наименование'].notna()]
sheet_goods = sheet_goods[sheet_goods['Код'].notna()]
sheet_goods = sheet_goods[sheet_goods['Наименование'].notna()]
sheet_goods = sheet_goods[sheet_goods['Код Валюты (0="ГРН" 1="USD" 2="EUR")'].notna()]

xls_price = pd.ExcelFile("price.xls")
sheet_price = xls_price.parse(0)

result = pd.merge(sheet_price, sheet_goods,  right_on='Ключ наименование', left_on='Название', how='left')

writer = pd.ExcelWriter('merge.xlsx')
result.to_excel(writer, 'Sheet1', index=False)
writer.save()
